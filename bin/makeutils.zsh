
function title () {
    local titlevar lineblock
    local lineblock="-="
    print -v titlevar -P "%F{blue}$@%f"
    print -f "$lineblock%0.0s" {1..$((($COLUMNS/$#lineblock)))} ; echo
    print -f "%s\n" $titlevar
    print -f "$lineblock%0.0s" {1..$((($COLUMNS/$#lineblock)))} ; echo
}

function getparam() {
    dialog
}
