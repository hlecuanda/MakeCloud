
function isVncServerRunning () {
  servers=$(
    ssh trantor vncserver -list \
    | grep -Ee '^:' \
    | wc -l )
  (( $servers >= 1 )) && return 1 || return 0
}

function isVncClientRunning () {
  vncProcs=$(
    ps ax \
    | grep vncview \
    | grep -v grep \
    | wc -l  )
  (( $vncProcs >= 1  )) && return 1 || return 0
}

function areWeUsingVnc () {
  (( $(isVncServerRunning) && $(isVncClientRunning) )) \
    && return 1 || return 0
}
#  vim: set ft=zsh sw=2 tw=0 fdm=manual et :
