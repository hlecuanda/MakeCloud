#!/usr/bin/env make default
#  __  __       _             _                 _
# |  \/  | __ _| | _____  ___| | ___  _   _  __| |
# | |\/| |/ _` | |/ / _ \/ __| |/ _ \| | | |/ _` |
# | |  | | (_| |   <  __/ (__| | (_) | |_| | (_| |
# |_|  |_|\__,_|_|\_\___|\___|_|\___/ \__,_|\__,_|

# h@h-lo.me 20200116 171827 -0800 PST 1579223907 d(-_- )b...

# Variables{{{
ALL        = $(shell ls . )
AWK           = gawk
CURL         != which curl -s
ECHO          = print -P '%B %s %b'
FWRULES       = --rules=tcp:22,tcp:631,udp:5353,udp:60000-61000
GCFWRULES     = gcloud compute firewall-rules
HOSTNAME     != hostname
INSTALLDIR    = $(HOME)/.Makecloud
INSTALLED     = $(addprefix $(INSTALLDIR)/,$(ALL))
MYIP         != curl -s https://mezaops.appspot.com/knock/ | jq .ip
# MYIP         = $(shell curl -4 ifconfig.co )
MYIP         = $(shell curl -s -4 https://mezaops.appspot.com/knock/ | jq .ip)
SHELL         = zsh
TITL          = print -P '%F{blue}%s%f\n'
VMPARAMS      = --direction=INGRESS --priority=850 --network=default
VMPARAMS     += --action=ALLOW --target-tags=bastion

OLDRANGES    = $(GCFWRULES) describe $(FWNAME) --format=json | jq -c '.sourceRanges' | tr -d \"\[\]


ifdef TERMUX
TITL       = echo
else
TITL       = . $(INSTALLDIR)/makeutils.zsh ; title
endif

help::        # Show this message:wq
	@figlet Makecloud
	@$(TITL) 'Cloud tasks automation '

# }}}

install: $(HOME)/Makefile $(INSTALLED)

gather: root.mk
	# gathering local changes

include clouddns.mk
include project.mk
include storage.mk


# Firewall Related rules{{{
help::
	@$(TITL) "Cloud Firewall"
	@cat Makefile             |  \
		$(AWK) '/#2/ && !/AWK/ { gsub(/#2/,"-"); print  $0  }' \
		| sort

include abbrev.mk

whatismyip:  #2 Show your current external IP
	@$(TITL) "Current IP outside "
	$(ECHO) $(MYIP)
	-@sleep 60

ifndef GCCONFIG
setconfig: GCCONFIG=default
endif
setconfig:
	@$(TITL) "Setting default gcloud config"
	@echo "WAS:"
	gcloud config list
	gcloud config configurations activate $(GCCONFIG)
	@echo "-----------------------"
	@sleep 3s
	@echo "IS:"
	gcloud config list

createfw:    #2 Creates a firewall for the current ip
	@$(TITL) "Creating mobile firewall"
	$(GCFWRULES) create $(FWNAME) $(FWPARAMS) \
		$(VMPARAMS) $(FWRULES) --source-ranges=$(MYIP)

listfw:      #2 Lists the projects firewalls
	@$(TITL) "Listing curent firewall rules"
	$(GCFWRULES) list

describefw:  #2 describes this firewall
	$(GCFWRULES) describe $(FWNAME)

deletefw:    #2 deletes this firewall
	-$(GCFWRULES) delete --quiet $(FWNAME)

updatefw: | setconfig   #2 update this firewall
	@$(TITL) "Updating Firewall Rule"
	[[ $(MYIP) == "$(shell $(OLDRANGES))"  ]] \
	   || $(GCFWRULES) update $(FWNAME) --source-ranges=$(MYIP)
# }}}

%: ;

#  vim: set ft=make sw=4 tw=0 fdm=marker noet :
