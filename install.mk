#
# |\/| _.|  _  _| _     _|
# |  |(_||<(/_(_|(_)|_|(_|

#  _           _        _ _             _
# (_)_ __  ___| |_ __ _| | |  _ __ ___ | | __
# | | '_ \/ __| __/ _` | | | | '_ ` _ \| |/ /
# | | | | \__ \ || (_| | | |_| | | | | |   <
# |_|_| |_|___/\__\__,_|_|_(_)_| |_| |_|_|\_\

# h@h-lo.me 20200116 172753 -0800 PST 1579224473 d(-_- )b...
# Install scripts for Makecloud itself
#
#
root.mk: $(HOME)/Makefile
	cp $< $@

$(HOME)/Makefile:
	install -m 755 $< $@

$(INSTALLDIR):
	mkdir $@

$(INSTALLED): $(INSTALLDIR)/%: ./%
	install -m 755 $< $@

.makefiletest:
	GCCONFIG=medialib $(MAKE) .pulledtest

.pulledtest: | setconfig
	gcloud config list

