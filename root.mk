LISTNETWORKS  = wpa_cli list_networks | grep -Ee 'CURRENT'
TESTAPI       = a="$$(curl -ism 1 www.googleapis.com )";
TESTAPI      += print -f "%s" "."; sleep 1
VNCCMD        = xtigervncviewer localhost:1 via trantor
WIFINO       != $(LISTNETWORKS) | awk '{ print $$1 }'
WIFINAM      != $(LISTNETWORKS) | awk '{ print $$2 }' | tr -d \"
SHELL         = zsh
WPACLI_CMD    = wpa_cli
MAKEFLAGS     = --no-print-directory

.SUFFIXES     =

ifeq ($(WIFINO),0)
NEWIFI        = 1
FIREWALL      = ens
else
NEWIFI        = 0
FIREWALL      = alcazar
endif

.PHONY: default whatwifi switchwifi
.PHONY: wifi-alcazar wifi-nokia wifi-ens

default: ;

whatwifi:
	@print -f "Current WiFi: %s\n" $(WIFINAM)

wifi-ens: NEWIFI = 0
wifi-ens: FIREWALL = ens
wifi-ens: switchwifi

wifi-alcazar: NEWIFI = 1
wifi-alcazar: FIREWALL = alcazar
wifi-alcazar: switchwifi

trantor-vnc: VNCSERVER = trantor
trantor-vnc:: .startvnc

.startvnc: .Makecloud/vnc/$(VNCSERVER)

switchwifi:
	@$(MAKE) whatwifi
	@-sudo ip wlan0 down 2> /dev/null || true
	wpa_cli select_network $(NEWIFI) 2>/dev/null
	@until [[ -n $$a ]] { $(TESTAPI) }
	@echo ..success!
	@$(MAKE) whatwifi
	$(MAKE) $(FIREWALL)

ens alcazar:
	$(MAKE) -C .Makecloud up$@

xres: .Xresources
	xrdb -load /etc/X11/Xresources/x11-common
	cd /etc/X11/app-defaults && \
	for f in * ; | xrdb -merge $$f
	xrdb -merge .Xresources

VNCSTARTCMD = vncserver -localhost -SecurityTypes=none -BaseHttpServer=8880

.Makecloud/vnc/$(VNCSERVER):  | .Makecloud/vnc
	ssh $(VNCSERVER) $(VNCSTARTCMD) 2&> /dev/null \
		&& touch $@


define INCRONTAB
# async inotify calls for Makecloud targets
# vars are
# $$@ fspath $$# fname $$% textflags $$& numflags
# format is:
# fspath eventmask executable_with_args
/home/pi/.Makecloud/vnc IN_CREATE $(absmake) -C $(HOME) $#-remote-vnc

#  vim: set ft=incrontab sw=2 tw=0 fdm=manual noet :
endef

.Makecloud/incrontab.tab: | .Makecloud/vnc
	cat =($(INCRONTAB)) >>! $@
	incrontab $@

.Makecloud/vnc:
	mkdir -p $@

.Xresources: ;
#  vim: set ft=make sw=4 tw=0 fdm=manual noet :
