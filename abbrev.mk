
# Aliases {{{
dsfw: describefw ;

lsfw: listfw ;

mkfw: createfw ;

rmfw: deletefw ;

upfw: updatefw ;

fw-ens: mkens ;

fw-alcazar: mkalcazar ;

# }}}
# frequent targets# {{{

mkens: FWNAME=ens-main
mkens: createfw

rmens: FWNAME=ens-main
rmens: rmfw

upens: FWNAME=ens-main
upens: updatefw

mkalcazar: FWNAME=ens-alcazar
mkalcazar: createfw

upalcazar: FWNAME=ens-alcazar
upalcazar: updatefw

rmalcazar: FWNAME=ens-alcazar
rmalcazar: rmfw

# }}}
#  vim: set ft=make sw=2 tw=0 fdm=marker noet :
