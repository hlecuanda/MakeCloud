

DNSOPT        = --type=A --ttl=300
ZONE          = --zone mcpaints-public
DNSPROJ       = lecuanda-domain-management
GDNS          = gcloud --project= $(DNSPROJ) dns record-sets transaction

help::
	@$(TITL) 'Cloud DNS Services'
	@cat Makefile             |      \
		$(AWK) '/#1/ && !/AWK/ { gsub(/#1/,"-"); print  $0  }' \
		| sort
	# }}}

# DNS Rules  {{{
setdns:      #1 set this host DNS
	$(GDNS) start $(ZONE)
	$(GDNS) remove --name=$(HOSTNAME) $(ZONE) $(DNSOPT)
	$(GDNS) add --name=$(HOSTNAME) --data=$(MYIP) $(ZONE)
	$(GDNS) describe $(ZONE)
	$(GDNS) execute $(ZONE)

abdns:       #1 abort a transaction
	-$(GDNS) abort $(ZONE)
# }}}
